$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    autoplay: 1000,
    dots: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
        },
        320:{
            items:1,
        },
        576:{
            items:2,
        },
        786:{
            items:3,  
        },
        1000:{
            items:4,
        }
    }
})